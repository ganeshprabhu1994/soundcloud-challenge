# SoundCloud Challenge Solution


##Introduction

Two ways in which the problem is solved

1. A Python & Scala solution following map-reduce paradigm.
2. A simple algorithmic Scala solution.

Initially coded the python solution as it is easy to get it run/test on any machine. 
Then implemented the same in Scala with minor differences.
Finally implemented a simple algorithmic solution in Scala


##Running Python Solution

To run,

```
$ ./src/main/python/mr.sh -i=$PWD/testcase/input -n=2 -o=$PWD/output
```

*    n - Nth degree connection
*    i - input file
*    o - output file

Above command expands as below

`$ cat $PWD/testcase/input | ./src/main/python/mapper_adjacents.py | sort -k1,1 | ./src/main/python/reducer_adjacents.py | ./src/main/python/mapper_loop.py | sort -k1,1 | ./src/main/python/reducer_loop.py | ./src/main/python/mapper_result.py > $PWD/output`

###Steps:
   1. Builds AdjacencyList `mapper_adjacents.py + reducer_adjacents.py` ( Iteration=1 ).
   2. Applies `mapper_loop.py + reducer_loop.py` subsequently N-1 times.
   3. Finally to get the result in the required format apply `mapper_result.py`.



##Running Python Solution using Hadoop Streaming API

```
$ ./src/main/python/mr-stream.sh -i=input_file_in_hdfs -n=2 -o=output_file
```

*    n - Nth degree connection
*    i - input file in hdfs
*    o - output file in local FS where you want the final result needs to be merged and stored

*Note: Assumes HADOOP_HOME is defined and HDFS is running. Uses hadoop-streaming-2.3.0-cdh5.0.0.jar 
( Please modify the HADOOP_STREAMING_JAR in mr-stream.sh, If you want a different version )*


##Running Scala MR solution

Running the below commands will give output for the given testcase with N=2

```
$ sbt assembly
$ cat ./testcase/input \
 | java -cp /soundcloud-challenge/target/scala-2.10/soundcloud-challenge-assembly-1.0.jar mr.MapperAdjacents \
 | sort -k1,1 \
 | java -cp /soundcloud-challenge/target/scala-2.10/soundcloud-challenge-assembly-1.0.jar mr.ReducerAdjacents \
 | java -cp /soundcloud-challenge/target/scala-2.10/soundcloud-challenge-assembly-1.0.jar mr.MapperLoop \
 | sort -k1,1 \
 | java -cp /soundcloud-challenge/target/scala-2.10/soundcloud-challenge-assembly-1.0.jar mr.ReducerLoop \
 | java -cp /soundcloud-challenge/target/scala-2.10/soundcloud-challenge-assembly-1.0.jar mr.MapperResult
 
```
*Note: Not tested against Streaming API(although it should work)*


##Running Scala Algo solution

Running the below commands will give output for the given testcase with N=2

```
$ sbt assembly
$ cat ./testcase/input \
 | java -cp /soundcloud-challenge/target/scala-2.10/soundcloud-challenge-assembly-1.0.jar algo.SoundcloudChallenge 2
 
```
*    **Time Complexity:**
    For finding all connections from a node will take `O(E)`. Since we limit only to a certain depth, this will turn out to
    average number of outdegrees per node `O(B)`. But We are doing it for every node that leads us to `O(|V|*|B|)` 

*    **Space Complexity:** `O(V+E)` for adjacency list

##MapReduce Algorithm and Correctness

###Basic Idea
*At the end of every iteration(i), for every node say X, we should have a vector(V), in which each node shares a 1 to i degree connections with X.
*

###Terms
*Adjacents(X) - All Nodes which are one hop away from node X*

###Algo and Correctness

```

    Base (Iteration=1):
        Building the Adjacency list ( For every node X in graph, we have vector V(v1,v2,...) )
            where v1,v2,... belongs to Adjacents(X)

    ...
    ...
    ...

    Iteration=N:
        Assume, For every node X in graph, we have vector V(v1,v2,...) which are 1 to N hops away from X

    Iteration=N+1:
        The reasoning goes like this,
            All connections which are N hops away from X in iteration=N, are atmost N+1 hops away from 1st degree connections of X i.e Adjacents(X)
            i.e For any node Y, the N+1 degree connections are those which are N degrees apart from the Adjacents(Y)

        Mapper:
            for every node X in the graph
                for every Y in Adjacents(X)
                    emit (Y, connections of X)
        Reducer: GroupBy Y and aggregate and dedup the connections
        
        Thus at the end of the iteration, for any node Y, we have all connections ranging from(1 to N+1), satisfying our Basic Idea/assumption

    So on and so forth similarly for N+2,N+3,...

```
*Note: There is no state maintained between iterations*


#Notes

- Overall it was a good exercise. I enjoyed it thoroughly. I tried to establish the correctness in a formal way. Please apologize if it's wrong.