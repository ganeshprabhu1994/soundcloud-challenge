#!/usr/bin/python

import sys
from user import User
from helper import *

for line in sys.stdin:
    person1,person2 = parse_to_tuple2(line)
    emit(person1, person2, '\t')
    emit(person2, person1, '\t')
