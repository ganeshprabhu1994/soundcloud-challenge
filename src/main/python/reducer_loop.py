#!/usr/bin/python

import sys
from user import User
from helper import *

current_user = User()

for line in sys.stdin:
    user = parse_line_to_user(line)

    if current_user == user:
        current_user.adjacents = current_user.adjacents.union(user.adjacents)
        current_user.connections = current_user.connections.union(user.connections)
    else:
        if current_user.name:
            emit(current_user.name, current_user.to_json())

        current_user = user

emit(current_user.name, current_user.to_json())
