#!/usr/bin/python

import sys
from user import User
from helper import *

for line in sys.stdin:
    user = parse_line_to_user(line)
    emit(user.name, '\t'.join(sorted(user.getAllConnections())), '\t')
