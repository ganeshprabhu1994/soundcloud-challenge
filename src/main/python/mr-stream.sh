#!/usr/bin/env bash

USAGE="[Usage]\n ./mr-stream.sh --nth-degree=2 --input=input-file-in-hdfs --output=output-file"

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ -z $HADOOP_HOME ]; then
    HADOOP_EXECUTABLE="hadoop";
else
    HADOOP_EXECUTABLE="${HADOOP_HOME}/bin/hadoop";
fi

HADOOP_STREAMING_JAR=$CURRENT_DIR/hadoop-streaming-2.3.0-cdh5.0.0.jar
LABEL=$(date +%s)
OUTPUT_DIR=/${LABEL}_iter_1
FILE_OPTS="-file ${CURRENT_DIR}/mapper_adjacents.py \
-file ${CURRENT_DIR}/reducer_adjacents.py \
-file ${CURRENT_DIR}/user.py \
-file ${CURRENT_DIR}/helper.py \
-file ${CURRENT_DIR}/mapper_loop.py \
-file ${CURRENT_DIR}/reducer_loop.py \
-file ${CURRENT_DIR}/mapper_result.py"


if [ "$#" -ne 3 ]; then
  echo $USAGE
  exit 1
fi


for i in "$@"
do
case $i in
    -n=*|--nth-degree=*)
    NUM_ITERATIONS="${i#*=}"
    shift
    ;;
    -i=*|--input=*)
    INPUT="${i#*=}"
    shift
    ;;
    -o=*|--output=*)
    OUTPUT="${i#*=}"
    shift
    ;;
    *)
    echo "Found invalid/incorrect option"
    echo $USAGE
    exit 1
    ;;
esac
done

function spin_job {
    $HADOOP_EXECUTABLE jar $HADOOP_STREAMING_JAR \
        -mapper $1 \
        -reducer $2 \
        $FILE_OPTS \
        -input $3 \
        -output $4
}

# Building the adjacency list
spin_job $CURRENT_DIR/mapper_adjacents.py $CURRENT_DIR/reducer_adjacents.py $INPUT $OUTPUT_DIR


for (( iteration=2; iteration<=$NUM_ITERATIONS; iteration++ ))
do
    spin_job $CURRENT_DIR/mapper_loop.py $CURRENT_DIR/reducer_loop.py $OUTPUT_DIR  /${LABEL}_iter_${iteration}
    OUTPUT_DIR=/${LABEL}_iter_${iteration}
done

# Simple Transform from JSON to required output format
spin_job $CURRENT_DIR/mapper_result.py org.apache.hadoop.mapred.lib.IdentityReducer $OUTPUT_DIR /${LABEL}_output

$HADOOP_EXECUTABLE fs -getmerge /${LABEL}_output $OUTPUT

