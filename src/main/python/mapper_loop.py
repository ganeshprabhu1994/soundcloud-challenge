#!/usr/bin/python

import sys
from user import User
from helper import *

for line in sys.stdin:
    user = parse_line_to_user(line)

    for adj in user.adjacents:
        allConnections = user.getAllConnections()
        allConnections.remove(adj)
        emit(adj, User(adj, set([user.name]), allConnections).to_json())
