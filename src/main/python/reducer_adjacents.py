#!/usr/bin/python

import sys
from user import User
from helper import *

current_user = None
adjacents_of_current_user = set()

for line in sys.stdin:
    person1, person2 = parse_to_tuple2(line)
    if current_user == person1:
        adjacents_of_current_user.add(person2)
    else:
        if current_user:
            emit(current_user, User(current_user, adjacents_of_current_user).to_json())

            current_user = None
            adjacents_of_current_user = set()

        current_user = person1
        adjacents_of_current_user.add(person2)

emit(current_user, User(current_user, adjacents_of_current_user).to_json())
