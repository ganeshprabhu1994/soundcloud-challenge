#!/usr/bin/python

import json

class User:
    def __init__(self, name = None, adjacents = set(), connections = set()):
        self.name = name
        self.adjacents = adjacents
        self.connections = connections

    def to_json(self):
        return json.dumps(self, default=lambda self: list(self) if isinstance(self, set) else self.__dict__,
                          sort_keys=True)

    def __eq__(self, other):
        return (isinstance(other, self.__class__)
                and self.name == other.name)

    def __ne__(self, other):
        return not self.__eq__(other)

    def getAllConnections(self):
        return self.adjacents.union(self.connections)

