#!/usr/bin/python

import json
from user import User

# Parse a line and constructs User object
def parse_line_to_user(input, seperator="=>"):
    key, value = input.split(seperator)
    parsed_json = json.loads(value.strip())
    connections = set()
    adjacents = set()
    if 'connections' in parsed_json:
        connections = set(parsed_json["connections"])

    adjacents = set(parsed_json["adjacents"])

    return User(parsed_json["name"], adjacents, connections)


def parse_to_tuple2(input, seperator='\t'):
    return input.strip().split(seperator)


def emit(key, value, seperator="=>"):
    print "%s%s%s" % (key, seperator, value)
