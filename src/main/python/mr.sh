#!/bin/sh

USAGE="[Usage]\n ./mr.sh --nth-degree=2 --input=input-file --output=output-file"

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "$#" -ne 3 ]; then
  echo $USAGE
  exit 1
fi


for i in "$@"
do
case $i in
    -n=*|--nth-degree=*)
    NUM_ITERATIONS="${i#*=}"
    shift
    ;;
    -i=*|--input=*)
    INPUT="${i#*=}"
    shift
    ;;
    -o=*|--output=*)
    OUTPUT="${i#*=}"
    shift
    ;;
    *)
    echo "Found invalid/incorrect option"
    echo $USAGE
    exit 1
    ;;
esac
done

input_file_iteration="${INPUT}_iter1"
cat $INPUT | python $CURRENT_DIR/mapper_adjacents.py  | sort -k1,1 | python $CURRENT_DIR/reducer_adjacents.py > $input_file_iteration

for (( iteration=2; iteration<=$NUM_ITERATIONS; iteration++ ))
do
	cat $input_file_iteration | python $CURRENT_DIR/mapper_loop.py | sort -k1,1 | python $CURRENT_DIR/reducer_loop.py > "${INPUT}_iter${iteration}"
	input_file_iteration="${INPUT}_iter${iteration}"
done

cat $input_file_iteration | python $CURRENT_DIR/mapper_result.py > $OUTPUT
rm ${INPUT}_iter*
