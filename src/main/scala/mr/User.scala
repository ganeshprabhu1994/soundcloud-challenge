package mr

import scala.collection.immutable.SortedSet

case class User(name: String, adjacents: Set[String] = Set.empty, connections: Set[String] = Set.empty) {
  def getAllConnections = adjacents ++ connections

  def prettyPrint = {
    val sortedConnections = getAllConnections.toList.sorted
    val connectionsString = sortedConnections.mkString("\t")
    s"$name\t$connectionsString"
  }
}
