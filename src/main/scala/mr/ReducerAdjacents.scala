package mr

import UserParser._
import Util._

import scala.collection.mutable.Set
import scala.io.Source

object ReducerAdjacents {
  var currentUser: Option[String] = None
  var currentUserAdjacents = Set[String]()

  def reducer: String => List[String] = { line =>
    val (person1, person2) = parseToTuple2(line)
    currentUser match {
      case Some(name) if name == person1 =>
        currentUserAdjacents += person2
        List.empty[String]
      case Some(name) =>
        val result = List(User(name, currentUserAdjacents.toSet))
        currentUser = Some(person1)
        currentUserAdjacents = Set(person2)
        result
      case None =>
        currentUser = Some(person1)
        currentUserAdjacents += person2
        List.empty[String]
    }
  }

  def main(args: Array[String]) = {
    Source.fromInputStream(System.in).getLines().flatMap(reducer).foreach(Util.emit)
    emit(User(currentUser.get, currentUserAdjacents.toSet))
  }
}


