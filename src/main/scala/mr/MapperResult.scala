package mr

import scala.io.Source
import Util._
import UserParser._

object MapperResult {
  def mapper: String => List[String] = { line =>
    UserParser(line) match {
      case Some(user) => List(user.prettyPrint)
      case None => List.empty
    }
  }

  def main(args: Array[String]) = {
    Source.fromInputStream(System.in).getLines().flatMap(mapper).foreach(Util.emit)
  }
}