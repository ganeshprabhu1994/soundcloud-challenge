package mr

object Util {
  def parseToTuple2(line: String, seperator: String = "=>") = {
    val arr = line.split(seperator)
    (arr(0), arr(1))
  }

  def emit: String => Unit = println(_)

  def getKV(k: String, v: String, seperator: String = "=>") = s"$k$seperator$v"
}
