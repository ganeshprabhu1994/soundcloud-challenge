package mr

import mr.UserParser._
import mr.Util._

import scala.io.Source

object ReducerLoop {
  var currentUser: Option[User] = None

  def reducer: String => List[String] = { line =>
    UserParser(line) match {
      case Some(user) =>
        currentUser match {
          case Some(current) if current.name == user.name =>
            currentUser = Some(current.copy(adjacents = user.adjacents ++ current.adjacents, connections = user.connections ++ current.connections))
            List.empty
          case Some(current) =>
            val result = List(current)
            currentUser = Some(user)
            result
          case None =>
            currentUser = Some(user)
            List.empty
        }
      case None => List.empty
    }
  }

  def main(args: Array[String]) = {
    Source.fromInputStream(System.in).getLines().flatMap(reducer).foreach(Util.emit)
    currentUser match {
      case Some(user) => emit(user)
      case None => {}
    }
  }

}