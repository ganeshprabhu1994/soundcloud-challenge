package mr

import scala.util.parsing.combinator.RegexParsers

object UserParser extends RegexParsers {
  def word = """\w+""".r

  def list: Parser[List[String]] = "[" ~> repsep(word, ",") <~ "]" ^^ { words => words }

  def userExpression: Parser[User] = (
    word ~ "=>" ~ list ~ list ^^ {
      case name ~ seperator ~ adjacents ~ connections => User(name, adjacents.toSet, connections.toSet)
    }
      | word ~ "=>" ~ list ^^ {
      case name ~ seperator ~ adjacents => User(name, adjacents.toSet)
    })

  implicit def userToKV(user: User): String = {
    user.name + "=>" + user.adjacents.mkString("[", ",", "]") + user.connections.mkString("[", ",", "]")
  }

  implicit def userListToStringList(lst: List[User]): List[String] = lst.map(userToKV)


  def apply(input: String): Option[User] = parseAll(userExpression, input) match {
    case Success(result, _) => Some(result)
    case NoSuccess(_, _) => None
  }
}