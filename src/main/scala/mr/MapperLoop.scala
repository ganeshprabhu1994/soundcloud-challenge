package mr

import Util._
import UserParser._

import scala.io.Source

object MapperLoop {
  def mapper: String => List[String] = { line =>
    UserParser(line) match {
      case Some(user) =>
        val allConnections = user.getAllConnections
        user.adjacents.map { adjacent => User(adjacent, Set(user.name), allConnections - adjacent) }.toList
      case None => List.empty
    }
  }

  def main(args: Array[String]) = {
    Source.fromInputStream(System.in).getLines().flatMap(mapper).foreach(Util.emit)
  }
}