package mr

import Util._

import scala.io.Source

object MapperAdjacents {
  def mapper: String => List[String] = { line =>
    val (person1, person2) = parseToTuple2(line, "\t")
    List(
      getKV(person1, person2),
      getKV(person2, person1))
  }

  def main(args: Array[String]) = {
    Source.fromInputStream(System.in).getLines().flatMap(mapper).foreach(Util.emit)
  }
}