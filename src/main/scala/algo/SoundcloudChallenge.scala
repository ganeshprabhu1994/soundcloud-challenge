package algo

import scala.io.Source
import scala.collection._

object SoundcloudChallenge {

  def solve(iterator: Iterator[String], n: Int) = {
    val graph = Graph.buildFrom(iterator)
    graph.findUptoNthDegreeConnectionsOfAll(n)
  }

  def main(args: Array[String]) {
    val result = solve(Source.fromInputStream(System.in).getLines(), args(0).toInt)
    result.toList.sortBy(_._1).map { case (k, v) => s"$k\t${v.sorted.mkString("\t")}" }.foreach(Util.emit)
  }
}
