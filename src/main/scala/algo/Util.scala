package algo

object Util {
  def parseTuple2(line: String, seperator: String = "\t") = {
    val arr = line.split(seperator)
    (arr(0), arr(1))
  }

  def emit: String => Unit = println(_)
}