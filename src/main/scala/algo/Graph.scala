package algo

import scala.collection.{mutable, Iterator}
import Util._


class Graph {
  private var adjacencyList = mutable.Map[String, List[String]]()

  case class NodeAndDegreesAway(node: String, degree: Int)

  def addRoute(from: String, to: String) = {
    adjacencyList += (from -> (to :: adjacencyList.getOrElse(from, List.empty)))
  }

  def findUptoNthDegreeConnectionsOf(n: Int, node: String): List[String] = {
    val queue = mutable.Queue[NodeAndDegreesAway](NodeAndDegreesAway(node, 0))
    val visited = mutable.Set[String](node)

    while (queue.nonEmpty && queue.front.degree <= n) {
      val dequedNode = queue.dequeue()
      if (dequedNode.degree < n)
        adjacents(dequedNode.node).foreach(adj =>
          if (!visited.contains(adj)) {
            queue += NodeAndDegreesAway(adj, dequedNode.degree + 1)
            visited += adj
          }
        )
    }

    (visited - node).toList
  }

  def findUptoNthDegreeConnectionsOfAll(n: Int): Map[String, List[String]] = {
    vertexes.map(v => (v, findUptoNthDegreeConnectionsOf(n, v))).toMap
  }

  def vertexes = adjacencyList.keys.toList

  def adjacents: String => List[String] = adjacencyList(_)
}

object Graph {
  def buildFrom: Iterator[String] => Graph = { iterator =>
    val graph = new Graph()
    iterator.foreach { line =>
      val (person1, person2) = parseTuple2(line)
      graph.addRoute(person1, person2)
      graph.addRoute(person2, person1)
    }
    graph
  }
}

